import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/system';
import SwipeableTextMobileStepper from "./SwipeableTextMobileStepper"

const Item = styled(Box)(({ theme }) => ({
  height: "100%",
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.primary,
}));

export default function SectionEmpat() {
  return (
    <div>
      <Item >
        <Container sx={{ mx:3,textAlign: 'center',}}>
          <h1>Testimonials</h1>
          <h4>Berbagai review positif dari para pelanggan kami</h4>
        </Container>
      </Item>

      <Grid container sx={{ flexDirection: { xs: "column-reverse", md: "row"} }} spacing={0}>
        <Grid item md={1}>
        <Item></Item>
        </Grid>  
        <Grid item md>
            <Item>
            <SwipeableTextMobileStepper/>  
            </Item>
        </Grid>
        <Grid item md={1}>
          <Item></Item>
        </Grid>
      </Grid>
    </div>
  );
}
