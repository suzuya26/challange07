import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/system';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import CheckCircleRoundedIcon from '@mui/icons-material/CheckCircleRounded';

const Item = styled(Box)(({ theme }) => ({
  height: "100%",
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.primary,
}));


export default function SectionDua() {
  return (
      <Grid container sx={{ flexDirection: { xs: "column-reverse", md: "row"} }} spacing={0}>
        <Grid item xs={12} md={6}>
          <Item >
            <Container sx={{ m:4,justifyContent: 'center' }}>
            <img src="./images/img_service.png" alt="img_services" style={{width:"67%"}}/>
            </Container>
          </Item>
        </Grid>
        <Grid item xs={12} md={6}>
            <Item>
              <Container sx={{ my:5, alignItems: 'center' }}>
              <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
              <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
              <Container>
                <List>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleRoundedIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Sewa Mobil Dengan Supir di Bali 12 Jam"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleRoundedIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Sewa Mobil Lepas Kunci di Bali 24 Jam"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleRoundedIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Sewa Mobil Jangka Panjang Bulanan"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleRoundedIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Gratis Antar - Jemput Mobil di Bandara"
                    />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleRoundedIcon />
                    </ListItemIcon>
                    <ListItemText
                      primary="Layanan Airport Transfer / Drop In Out"
                    />
                  </ListItem>
                </List>
              </Container>
              </Container>  
            </Item>
        </Grid>       
      </Grid>
  );
}
