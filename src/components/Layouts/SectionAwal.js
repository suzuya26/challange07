import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/system';
import Button from '@mui/material/Button';
import CarRentalIcon from '@mui/icons-material/CarRental';
import { Link } from 'react-router-dom';


const Item = styled(Box)(({ theme }) => ({
  height: "100%",
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.primary,
}));

const Tulisan = styled(Box)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.mode === 'dark' ? 'rgba(0, 0, 0, 0.87)' : '#fff',
}));

export default function SectionAwal() {
  return (
    <Item>
      <Grid container spacing={0}>
        <Grid item xs={12} md={6}>
            <Item>
              <Container sx={{ my:5,alignItems: 'center' }}>
              <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
              <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
              <Button variant="contained" endIcon={<CarRentalIcon />}>
              <Link to={"/listcars"} style={{ textDecoration: 'none'}}>
                <Tulisan>Sewa Sekarang</Tulisan>
                </Link>
              </Button>
              </Container>  
            </Item>
        </Grid>
        <Grid item xs={12} md={6}>
          <Item sx={{ justifyContent: 'flex-end' }}>
              <img src="./images/img_car.png" alt="img_car" style={{width:"90%"}}/>
          </Item>
        </Grid>
      </Grid>
    </Item>
  );
}
