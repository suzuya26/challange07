import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import { Container } from '@mui/system';

const Item = styled(Box)(({ theme }) => ({
  height: "100%",
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'left',
  color: theme.palette.text.primary,
}));

const ItemPaper = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#0288d1' : '#4fc3f7',
  ...theme.typography.body2,
  textAlign: 'center',
  color: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  height: '100%',
  borderRadius: 25,
}));

export default function SectionLima() {
  return (
    <div>
      <Item>
        <Item sx={{ my:3}}>
          <Container sx={{ mx:3,textAlign: 'center',}}>
            <ItemPaper sx={{ py:10}}>
            <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            </ItemPaper>
          </Container>
        </Item>
      </Item>
    </div>
  );
}
