import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import { Container } from "@mui/system";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import RecommendIcon from '@mui/icons-material/Recommend';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import AccessTimeFilledIcon from '@mui/icons-material/AccessTimeFilled';
import StarsIcon from '@mui/icons-material/Stars';

const Item = styled(Box)(({ theme }) => ({
  height: "100%",
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "left",
  color: theme.palette.text.primary,
}));

export default function SectionDua() {
  return (
    <div>
      <Item>
        <Container sx={{ mx: 3, alignItems: "center" }}>
          <h1>Why Us?</h1>
        </Container>
      </Item>
      <Grid
        container
        sx={{ flexDirection: { xs: "column", md: "row" } }}
        spacing={0}
      >
        <Grid item xs={12} md={6} lg>
          <Item>
            <Container sx={{ my: 2, alignItems: "center" }}>
              <Card sx={{ Width: "100%" }}>
                <CardContent>
                  <Grid container sx={{ flexDirection: { xs: "row", md: "column" } }} spacing={2}>
                    <Grid item >
                      <RecommendIcon sx={{ fontSize: 80 }}/>
                    </Grid>
                    <Grid item >
                    <Typography variant="h5">
                Mobil Lengkap
                  </Typography>
                  <Typography variant="body1">
                  Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                  </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Container>
          </Item>
        </Grid>
        <Grid item xs={12} md={6} lg>
          <Item>
            <Container sx={{ my: 2, alignItems: "center" }}>
            <Card sx={{ Width: "100%" }}>
                <CardContent>
                  <Grid container sx={{ flexDirection: { xs: "row", md: "column" } }} spacing={2}>
                    <Grid item >
                      <MonetizationOnIcon sx={{ fontSize: 80 }}/>
                    </Grid>
                    <Grid item >
                    <Typography variant="h5">
                    Harga Murah
                  </Typography>
                  <Typography variant="body1">
                  Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat
                  </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Container>
          </Item>
        </Grid>
        <Grid item xs={12} md={6} lg>
          <Item>
            <Container sx={{ my: 2, alignItems: "center" }}>
            <Card sx={{ Width: "100%" }}>
                <CardContent>
                  <Grid container sx={{ flexDirection: { xs: "row", md: "column" } }} spacing={2}>
                    <Grid item >
                      <AccessTimeFilledIcon sx={{ fontSize: 80 }}/>
                    </Grid>
                    <Grid item >
                    <Typography variant="h5">
                    Layanan 24 Jam
                  </Typography>
                  <Typography variant="body1">
                  Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu
                  </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Container>
          </Item>
        </Grid>
        <Grid item xs={12} md={6} lg>
          <Item>
            <Container sx={{ my: 2, alignItems: "center" }}>
            <Card sx={{ Width: "100%" }}>
                <CardContent>
                  <Grid container sx={{ flexDirection: { xs: "row", md: "column" } }} spacing={2}>
                    <Grid item >
                      <StarsIcon sx={{ fontSize: 80 }}/>
                    </Grid>
                    <Grid item >
                    <Typography variant="h5">
                    Sopir Profesional
                  </Typography>
                  <Typography variant="body1">
                  Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu
                  </Typography>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Container>
          </Item>
        </Grid>
      </Grid>
    </div>
  );
}
