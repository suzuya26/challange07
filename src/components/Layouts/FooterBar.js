import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { Container } from "@mui/system";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemButton from "@mui/material/ListItemButton";
import Divider from "@mui/material/Divider";
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import EmailIcon from '@mui/icons-material/Email';
import YouTubeIcon from '@mui/icons-material/YouTube';

export default function FooterBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar sx={{ my: 3 }}>
          <Grid container>
            <Grid item xs={12} md={6} lg={3}>
              <Typography
                component="div"
                sx={{ flexGrow: 1, textAlign: "left" }}
              >
                <p>Jalan Suroyo No. 161 Mayangan</p>
                <p>Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
              </Typography>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <List>
                <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemText primary="Our Services" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton component="a" href="#simple-list">
                    <ListItemText primary="Why Us" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton component="a" href="#simple-list">
                    <ListItemText primary="Testimonials" />
                  </ListItemButton>
                </ListItem>
                <ListItem disablePadding>
                  <ListItemButton component="a" href="#simple-list">
                    <ListItemText primary="FAQ" />
                  </ListItemButton>
                </ListItem>
              </List>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <Typography
                component="div"
                sx={{ flexGrow: 1, mx: 3, textAlign: "left" }}
              >
                <p>Connect With Us</p>
              </Typography>
              <Container sx={{textAlign : 'left'}}>
                  <FacebookIcon fontSize="large"/>
                  <InstagramIcon fontSize="large"/>
                  <TwitterIcon fontSize="large"/>
                  <EmailIcon fontSize="large"/>
                  <YouTubeIcon fontSize="large"/>
              </Container>
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <Typography
                component="div"
                sx={{ flexGrow: 1, textAlign: "left" }}
              >
                <p>Copyright Binar 2022</p>
              </Typography>
              <Typography
                variant="h1"
                noWrap
                component="div"
                sx={{ flexGrow: 1,textAlign: "left" }}
              >
                BNC
              </Typography>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
