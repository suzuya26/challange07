import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import { useTheme, ThemeProvider, createTheme } from '@mui/material/styles';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import Fab from '@mui/material/Fab';
import ResponsiveAppBar from '../Layouts/ResponsiveAppBar';
import ListCars from '../ListCars';
import FooterBar from '../Layouts/FooterBar';




const ColorModeContext = React.createContext({ toggleColorModeCar: () => {} });
const stail= {
  margin: 0,
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
};
function MyApp() {
  const theme = useTheme();
  const colorMode = React.useContext(ColorModeContext);
  return (
    <Box style={stail}  sx={{ '& > :not(style)': { m: 1 } }} >
      <Fab color="inherit" aria-label="add" className="fab" >
        <IconButton  onClick={colorMode.toggleColorModeCar} color="inherit">
        {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
      </IconButton>
      </Fab>
    </Box>
  );
}

export default function ToggleColorModeCar() {
  const [mode, setMode] = React.useState('light');
  const colorMode = React.useMemo(
    () => ({
      toggleColorModeCar: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [],
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
      <header>
      <ResponsiveAppBar/>
      </header>
      <ListCars/>
      <FooterBar/>
      <MyApp />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
