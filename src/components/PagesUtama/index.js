import * as React from 'react';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import { useTheme, ThemeProvider, createTheme } from '@mui/material/styles';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import ResponsiveAppBar from '../Layouts/ResponsiveAppBar';
import Fab from '@mui/material/Fab';
import SectionAwal from '../Layouts/SectionAwal';
import SectionDua from '../Layouts/SectionDua';
import SectionTiga from '../Layouts/SectionTiga';
import SectionEmpat from '../Layouts/SectionEmpat';
import SectionLima from '../Layouts/SectionLima';
import SectionEnam from '../Layouts/SectionEnam';
import FooterBar from '../Layouts/FooterBar';


const ColorModeContext = React.createContext({ toggleColorMode: () => {} });
const stail= {
  margin: 0,
  top: 'auto',
  right: 20,
  bottom: 20,
  left: 'auto',
  position: 'fixed',
};
function MyApp() {
  const theme = useTheme();
  const colorMode = React.useContext(ColorModeContext);
  return (
    <Box style={stail}  sx={{ '& > :not(style)': { m: 1 } }} >
      <Fab color="inherit" aria-label="add" className="fab" >
        <IconButton  onClick={colorMode.toggleColorMode} color="inherit">
        {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
      </IconButton>
      </Fab>
    </Box>
  );
}

export default function ToggleColorMode() {
  const [mode, setMode] = React.useState('light');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [],
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
      <header>
      <ResponsiveAppBar/>
      </header>
      <SectionAwal/>
      <SectionDua/>
      <SectionTiga/>
      <SectionEmpat/>
      <SectionLima/>
      <SectionEnam/>
      <FooterBar/>
      <MyApp />
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}
