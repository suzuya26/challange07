import './App.css';
import { ToggleColorMode } from './components';

function App() {
  return (
    <div className="App">
      <ToggleColorMode/>
    </div>
  );
}

export default App;
